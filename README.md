# My First Repo!
## Getting Started with this project

To view this project, download the files and view the index.html file in the browser.

## LICENSE.md

### Why choose the MIT License?
After a small amount of research, it seems to be the recommended license among software developers that is business friendly, open source friendly, and allows for monetization. The conditions of use are simple, with the name and original copyright included in the license along with your own name and dates below